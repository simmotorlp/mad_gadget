//= partials/jquery.magnific-popup.min.js

$(function() {

	$('.popup').magnificPopup({
        showCloseBtn: false
    });

    $("a.scrollto").click(function() {
        var elementClick = $(this).attr("href")
        var destination = $(elementClick).offset().top;
        jQuery("html:not(:animated),body:not(:animated)").animate({
            scrollTop: destination
        }, 1000);
        return false;
    });

    $('.js_close_popup').click(function () {
        $.magnificPopup.close();
    });

});
